<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('company_id')->nullable();
            $table->integer('customer_id')->nullable();
            $table->integer('rider_id')->nullable();
            $table->integer('is_order_active')->default('0');
            $table->integer('is_paid')->default('0');
            $table->integer('is_delivered')->default('0');
            $table->string('order_number')->nullable();
            $table->string('tracking_number')->nullable();
            $table->string('product_image')->nullable();
            $table->string('product_name')->nullable();
            $table->string('quantity')->nullable();
            $table->string('size')->nullable();
            $table->string('weight')->nullable();
            $table->string('distance')->nullable();
            $table->string('color')->nullable();
            $table->string('price')->nullable();
            $table->string('discount')->nullable();
            $table->string('coupon_code')->nullable();
            $table->string('shipping_fee')->nullable();
            $table->string('service_type')->nullable();
            $table->string('delivery_contact_number')->nullable();
            $table->string('pickup_contact_number')->nullable();
            $table->string('pickup_date')->nullable();
            $table->string('pickup_time')->nullable();
            $table->string('drop_of_date')->nullable();
            $table->string('drop_of_time')->nullable();
            $table->string('status')->nullable();
            $table->string('pickup_location_address_line1')->nullable();
            $table->string('pickup_location_address_line2')->nullable();
            $table->string('pickup_location_country')->nullable();
            $table->string('pickup_location_parish')->nullable();
            $table->string('pickup_location_city')->nullable();
            $table->string('drop_of_location_address_line1')->nullable();
            $table->string('drop_of_location_address_line2')->nullable();
            $table->string('drop_of_location_country')->nullable();
            $table->string('drop_of_location_parish')->nullable();
            $table->string('drop_of_location_city')->nullable();
            $table->longText('description')->nullable();
            $table->longText('message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
