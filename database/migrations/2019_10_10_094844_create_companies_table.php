<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company_name')->nullable();
            $table->string('fullname')->nullable();
            $table->string('email')->unique();
            $table->string('contact_number')->unique();
            $table->string('phone_number')->unique();
            $table->text('address')->nullable();
            $table->string('country')->nullable();
            $table->string('parish')->nullable();
            $table->string('city')->nullable();
            $table->string('street')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->string('mr_ms')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('is_approved')->default('0');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
